<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Form\Type\ReportRevisionType;
use AppBundle\Entity\ReportRevision;
use AppBundle\Entity\Report;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class CompanyController extends Controller
{
    /**
     * @Route("/reports", name="app_report_list")
     *
     * @Template("AppBundle:Company:index.html.twig")
     *
     * @return array
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $approvedReports = $em->getRepository("AppBundle:ReportRevision")->getLatestApprovedReports();
        return [
            'reports' => $approvedReports
        ];
    }

    /**
     * @Route("/add", name="app_report_add")
     *
     * @Template("AppBundle:Company:form.html.twig")
     *
     * @return array
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $reportRevision = new ReportRevision();
        $reportRevision->setReport(new Report());
        $form = $this->createForm(new ReportRevisionType(), $reportRevision);
        $form->handleRequest($request);
        if($form->isValid()){
            $reportRevision = $form->getData();
            $em->persist($reportRevision);
            $em->flush();
            $this->get('session')->getFlashBag()->set('success', 'Your report display soon after approved by Manager');
            return $this->redirectToRoute('app_report_list');
        }
        return ['form' => $form->createView()];
    }

    /**
     * @Route("/edit/{id}", name="app_report_edit")
     *
     * @ParamConverter("report_revision", class="AppBundle:ReportRevision", options={"id" = "id"})
     *
     * @Template("AppBundle:Company:form.html.twig")
     *
     * @return array
     */
    public function editAction(Request $request, ReportRevision $reportRevision)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ReportRevisionType(), $reportRevision);
        $form->handleRequest($request);
        if($form->isValid()){
            $reportRevision = $form->getData();
            $newReportRevision = clone $reportRevision;
            $newReportRevision->setIsApproved(0);
            $em->persist($newReportRevision);
            $em->flush($newReportRevision);
            $this->get('session')->getFlashBag()->set('success', 'Edited report display soon after approved by Manager');
            return $this->redirectToRoute('app_report_list');
        }
        return ['form' => $form->createView()];
    }

    /**
     * @Route("/manage", name="app_report_manage")
     *
     * @Template("AppBundle:Company:manage.html.twig")
     *
     * @return array
     */
    public function manageAction()
    {
        $em = $this->getDoctrine()->getManager();

        $reportRequests = $em->getRepository("AppBundle:ReportRevision")->findBy(['isApproved' => 0], ['id' => 'DESC']);
        return [
            'reports' => $reportRequests
        ];
    }

    /**
     * @Route("/validate", name="app_report_validate", options={"expose"=true})
     *
     *
     * @return array
     */
    public function validateReportAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $reportRequests = $em->getRepository("AppBundle:ReportRevision")->find($request->get('validate_report'));
        if($request->get('approve_disapprove') == 1) {
            $reportRequests->setIsApproved($request->get('approve_disapprove'));
            $this->get('session')->getFlashBag()->set('success', 'Report approved successfully.');
            foreach($reportRequests->getReport()->getReportRevision() as $reportRevision){
                if($reportRevision == $reportRequests){
                    continue;
                }
                $em->remove($reportRevision);
            }
        }
        else {
            $this->get('session')->getFlashBag()->set('success', 'Report removed successfully.');
            $em->remove($reportRequests);
        }
        $em->flush();
        return new Response('success');
    }
}
