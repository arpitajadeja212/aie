$(document).ready(function(){
    $(".validate-btn").click(function(){
        $("#myModal").modal('show');
        $('#validate_report_id').val($(this).attr('data-id'));
        $('#model-type').html($(this).parents('tr').find('#report-type').html());
        $('#model-name').html($(this).parents('tr').find('#report-name').html());
        $('#model-description').html($(this).parents('tr').find('#report-description').html());
    });
    showHide($('.report-type').val());
    $('.report-type').on('change',function(){
        var type = $(this).val();
        $('#report_revision_type_latitude').val('');
        $('#report_revision_type_longitude').val('');
        $('#report_revision_type_additionalText').val('');
        showHide(type);
        
    });
    $('#save-button').click(function(){
        $.ajax({
            method: 'POST',
            url: Routing.generate('app_report_validate'),
            data : $('#report-form-validate').serialize(),
            success: function (data) {
                window.location.reload();
            }
        });
    });
});
function showHide(type){
    $('.report-type-div').hide();
    $('#'+type+'-div').show();
}