<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Report
 *
 * @ORM\Table(name="report")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ReportRepository")
 */
class Report
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=25)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ReportRevision", mappedBy="report")
     */
    private $reportRevision;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Report
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reportRevision = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add reportRevision
     *
     * @param \AppBundle\Entity\ReportRevision $reportRevision
     * @return Report
     */
    public function addReportRevision(\AppBundle\Entity\ReportRevision $reportRevision)
    {
        $this->reportRevision[] = $reportRevision;

        return $this;
    }

    /**
     * Remove reportRevision
     *
     * @param \AppBundle\Entity\ReportRevision $reportRevision
     */
    public function removeReportRevision(\AppBundle\Entity\ReportRevision $reportRevision)
    {
        $this->reportRevision->removeElement($reportRevision);
    }

    /**
     * Get reportRevision
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReportRevision()
    {
        return $this->reportRevision;
    }
}
