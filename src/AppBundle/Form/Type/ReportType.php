<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            $builder
                ->add('type', 'choice',
                    [
                        'required' => true,
                        'choices' => ['general' => 'General', 'site' => 'Site'],
                        'label' => false,
                        'attr' => ['class' => 'report-type']
                    ]
                );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Report',
        ));
    }

    public function getName()
    {
        return 'report_type';
    }
}