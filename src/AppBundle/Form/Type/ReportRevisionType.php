<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\Type\ReportType;

class ReportRevisionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            $builder
                ->add('name', 'text',
                    [
                        'required' => true
                    ]
                )
                ->add('report', new ReportType(),
                    [
                        'required' => true
                    ]
                )
                ->add('description', 'textarea',
                    [
                        'required' => true
                    ]
                )
                ->add('additionalText', 'textarea',
                    [
                        'required' => true,
                    ]
                )
                ->add('latitude', 'text',
                    [
                        'required' => true
                    ]
                )
                ->add('longitude', 'text',
                    [
                        'required' => true
                    ]
                )
                ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ReportRevision',
            'cascade_validation' => true,
        ));
    }

    public function getName()
    {
        return 'report_revision_type';
    }
}